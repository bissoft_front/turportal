/*
| ----------------------------------------------------------------------------------
| TABLE OF CONTENT
| ----------------------------------------------------------------------------------

-Preloader
-Datepicker
-Menu multilevel
-Scale images
-Select customization
-Zoom Images
-Slider
-Slider with thumbnails
-Form validation
-Input effects
*/



$(document).ready(function() {

  "use strict";



// PRELOADER //

    var $preloader = $('#page-preloader'),
    $spinner   = $preloader.find('.spinner-loader');
    $spinner.fadeOut();
    $preloader.delay(50).fadeOut('slow');

// DATEPICKER //

  if ($('.input-group.date').length) {
    $('.input-group.date').datepicker({
      language: "uk",
      daysOfWeekHighlighted: "0,6",
      todayHighlight: true
    });
  }



// SCALE IMAGES

  if ($('.img-scale').length) {
    $(function () { objectFitImages('.img-scale') });
  }


// SELECT CUSTOMIZATION


if ($('.select_box').length) {
   $(function() {
    var $select = $('.select_box').selectpicker({
      noneResultsText: "нічого не знайдено {0}"
    });

    $(':reset').on('click', function(evt) {
        evt.preventDefault();
        var $form = $(evt.target).closest('form');
        $form[0].reset();
        $form.find('select').selectpicker('render')
    });
  })
}


// ZOOM IMAGES //

  if ($('.js-zoom-gallery').length) {
      $('.js-zoom-gallery').each(function() {
          $(this).magnificPopup({
              delegate: '.js-zoom-gallery__item',
              type: 'image',
              gallery: {
                enabled:true
              },
        mainClass: 'mfp-with-zoom',

        zoom: {
          enabled: true,

          duration: 300,
          easing: 'ease-in-out',
          opener: function(openerElement) {
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
          });
      });
    }


  if ($('.js-zoom-images').length) {
      $('.js-zoom-images').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom',

        zoom: {
          enabled: true,

          duration: 300,
          easing: 'ease-in-out',
          opener: function(openerElement) {
            return openerElement.is('img') ? openerElement : openerElement.find('img');
          }
        }
      });

    }


  if ($('.js-popup-video').length) {
    $('.js-popup-video').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false,
      zoom: {
        enabled: true,
        duration: 300
      }
    });
  }


// SLIDER

  if ($('.js-slider').length) {
    $('.js-slider').slick();
  };



// SLIDER WITH THUMBNAILS

 $('.js-slider-thumb_main').slick({
  fade: true,
  asNavFor: '.js-slider-thumb_nav'
});
$('.js-slider-thumb_nav').slick({
  slidesToScroll: 1,
  asNavFor: '.js-slider-thumb_main',
  focusOnSelect: true
});


});





// FORM VALIDATION

window.addEventListener('load', function() {
  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.getElementsByClassName('needs-validation');
  // Loop over them and prevent submission
  var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });
}, false);



// INPUT EFFECTS

(function() {
    if (!String.prototype.trim) {
        (function() {
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function() {
                return this.replace(rtrim, '');
            };
        })();
    }

    [].slice.call( document.querySelectorAll( '.js-input' ) ).forEach( function( inputEl ) {
        if( inputEl.value.trim() !== '' ) {
            classie.add( inputEl.parentNode, 'ui-input_filled' );
        }
        inputEl.addEventListener( 'focus', onInputFocus );
        inputEl.addEventListener( 'blur', onInputBlur );
    } );

    function onInputFocus( ev ) {
        classie.add( ev.target.parentNode, 'ui-input_filled' );
    }

    function onInputBlur( ev ) {
        if( ev.target.value.trim() === '' ) {
            classie.remove( ev.target.parentNode, 'ui-input_filled' );
        }
    }
})();

