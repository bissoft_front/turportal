head: {
    defaults: {
        title: 'Індекс',
        useSocialMetaTags: false
    },
    home: {
        title: 'Головна',
        useSocialMetaTags: false
    },
    about: {
        title: 'Про проект',
        useSocialMetaTags: false
    },
    consultation: {
        title: 'Консультація',
        useSocialMetaTags: false
    },
    consultations: {
        title: 'Консультації',
        useSocialMetaTags: false
    },
    deputies: {
        title: 'Депутати',
        useSocialMetaTags: false
    },
    instructions: {
        title: 'Інструкції',
        useSocialMetaTags: false
    },
    law: {
        title: 'Нормативно-правова база',
        useSocialMetaTags: false
    },
    persona: {
        title: 'Депутат',
        useSocialMetaTags: false
    },
    news: {
        title: 'Новини',
        useSocialMetaTags: false
    },
    post: {
        title: 'Новина',
        useSocialMetaTags: false
    },
    boardComposition: {
        title: 'Склад Ради',
        useSocialMetaTags: false
    },
    rollcall: {
        title: 'Поіменне голосування',
        useSocialMetaTags: false
    }
}
