var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(48.30978575, 37.17038329);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoom: 13,
    scrollwheel: false
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(48.30978575, 37.17038329)
  });

  // var contentString =

  //   '<a class="b-map-bubble" href="#">' +
  //     '<div class="b-map-bubble__title">Длинный заголовок допонительной легенды вверху</div>' +
  //     '<div class="b-map-bubble__info">Умный термостат iDevices Thermostat поможет вам контролировать температуру у вас дома или в квартире с помощью бесплатного приложения.</div>' +
  //   '</a>' ;


  // infoBubble = new InfoBubble({
  //   minWidth: 382,
  //   minHeight: 130,
  //   content: contentString,
  //   shadowStyle: 1,
  //   padding: 0,
  //   arrowSize: 10,
  //   borderWidth: 0,
  //   arrowPosition: 50,
  //   borderRadius: 4
  // });

  // infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);
